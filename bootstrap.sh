#!/usr/bin/env bash

# This script is meant to be run on a new machine to configure it for
# development.  It is, therefore, idempotent in that invoking it more than once
# *should* not alter the system any further.
#
# Invoking this script will install Homebrew and a few system packages (if
# running on a Mac), followed by a system-wide install of a few globally useful
# Python packages.  Next, symlinks will be made from the dotfiles in this
# repository to the appropriate locations in the user's $HOME.  Finally, it
# will install the Vim plugin manager, Vundle, and use it to install the Vim
# plugins as defined the .vimrc file.


# If this is a Mac, let's install Homebrew and use it to install the latest
# versions of some useful packages.
if [[ $OSTYPE == darwin* ]]; then
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew install ack git git-flow readline rename tig vim watch cowsay sl
fi

# Install globally required Python files
sudo easy_install pip
sudo pip install virtualenv virtualenvwrapper flake8

# TODO: check for, and download PHPCS phars and make them accessible. Add custom ruleset.

# If symlinks to the various dotfiles do not already exist in the right
# locations, make them.
BASEPATH="$( cd "$(dirname "${BASH_SOURCE[0]}}" )" && pwd)"
for DOTFILE in bashrc bash_profile vimrc screenrc gitconfig git-completion.sh git-prompt.sh
do
    SOURCEFILE=$BASEPATH/$DOTFILE
    if [ ! -L ~/$DOTFILE ]; then
        ln -s $SOURCEFILE ~/.$DOTFILE
    fi
done

# If Vundle is not installed, install it and all the Vim plugins defined in the
# .vimrc.
VUNDLEDIR=~/.vim/bundle/Vundle.vim
if [ ! -d "$VUNDLEDIR" ]; then
    mkdir -p ~/.vim/bundle
    git clone https://github.com/gmarik/Vundle.vim.git $VUNDLEDIR
    vim +PluginInstall +qall
fi
