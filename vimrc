" vim > vi; must be first, because it changes other settings as a side effect
set nocompatible

filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Let vundle manage itself
Plugin 'gmarik/Vundle'

" Plugins
Plugin 'tpope/vim-commentary'             " [Un]Comment targets
Plugin 'tpope/vim-fugitive'               " Git interface
Plugin 'tpope/vim-abolish'                " Powerful pluralizing search-and-replace
Plugin 'tpope/vim-surround'               " For working with quotes, parens, and tags in pairs
Plugin 'tpope/vim-repeat'                 " . for plugins
Plugin 'davidhalter/jedi-vim'             " Python Auto-complete
Plugin 'ervandew/supertab'                " Use <TAB> for completion
Plugin 'kien/ctrlp.vim'                   " Fuzzy file/tag explorer
Plugin 'mileszs/ack.vim'                  " Ack grep replacement in Vim
Plugin 'scrooloose/nerdtree'              " Filebrowser
Plugin 'jistr/vim-nerdtree-tabs'          " Make NERDTree play nicely with vim tabs
Plugin 'scrooloose/syntastic'             " Syntax checker
Plugin 'Lokaltog/powerline'               " Enhanced status bar
Plugin 'mitsuhiko/vim-jinja'              " Jinja2 template support
Plugin 'sumpygump/vim-php-debugger'       " Because sometimes in life you do have to write PHP. :-(
Plugin 'AndrewRadev/splitjoin.vim'        " Struct split and join
Plugin 'SirVer/ultisnips'                 " Useful snippets
Plugin 'fatih/vim-go'                     " Go build/run, etc. support

call vundle#end()          " required
filetype plugin indent on  " required

" Change mapleader to ,
let mapleader=","

" open the syntastic fix window automatically
let g:syntastic_auto_loc_list=1

" set the working directory for CtrlP
let g:ctrlp_working_path_mode = 'ra'
nnoremap <leader>. :CtrlPTag<cr>
nnoremap <leader>o :CtrlP<cr>

" set keybindings for jedi-vim
let g:jedi#usages_command = '<leader>u'

" shortcuts for vim-commentary
nnoremap <leader>c :Commentary<cr>

" config for vim-php-debugger :-(
let g:dbgPavimPathMap = [['/Users/faldridge/projects/hipchat/web/www','/'],]

" Use jshint
let g:syntastic_javascript_checkers = ['jsxhint']

" Use flake8
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_python_flake8_args = '--ignore=E501 --max-line-length=120'

" Use Py3 - comment out for Python 2
" let g:syntastic_python_python_exec = '/usr/bin/python3.5'

" Necessary evil
let g:syntastic_php_checkers = ['phpcs']
let g:syntastic_php_phpcs_args = "--config-set tab_width 2 --standard=$HOME/projects/dotfiles/ruleset.xml"

" filter out a few unnecessary files from the nerd tree
let NERDTreeIgnore=['\.pyc$', '\~$']

" show hidden files in NERDTree
let NERDTreeShowHidden=1

" quickly toggle the NERDTree
" map <Leader>n <plug>NERDTreeTabsToggle<CR>

" Setting for Powerline plugin
let g:Powerline_symbols="fancy"

" Editing Behavior - {{{
set showmode                        " always show the current editing mode
set nowrap                          " do not wrap lines
set tabstop=4                       " tab == 4 spaces
set softtabstop=4                   " when backspacing, pretend a tab is removed
                                    " (4 spaces)
set expandtab                       " expand all tabs (to spaces) by default
set shiftwidth=4                    " number of spaces used for autoindenting
set shiftround                      " use a multiple of shiftwidth when using
                                    " < and >
set backspace=indent,eol,start      " backspace over everything in insert mode
set autoindent                      " always turn on autoindent
set copyindent                      " copy the previous indent when
                                    " autoindenting
set number                          " always show line numbers
set showmatch                       " highlight matching parens/braces
set ignorecase                      " ignore case when searching
set smartcase                       " do not ignore case when search pattern
                                    " is mixed-case
set smarttab                        " insert tabs on start of line according
                                    " to shiftwidth, not tabstop
set virtualedit=block,onemore       " allow the cursor anywhere
set hlsearch                        " highlight search terms
set incsearch                       " show matches as you type
set gdefault                        " search/replace with the /g flag by default
set listchars=tab:▸\ ,trail:·,extends:#,nbsp:·

set nolist                          " hide invisible characters by default
set pastetoggle=<F2>                " enable/disable paste mode
"set mouse=a                        " enable the mouse if the term supports it
set fileformats="unix,dos,mac"
set formatoptions+=1                " don't end lines with 1-letter words
                                    " when wrapping paragraphs

"Many people like to remove any extra whitespace from the ends of lines. Here
" is one way to do it when saving your file.
fun! <SID>StripTrailingWhitespaces()
  let l = line(".")
  let c = col(".")
  %s/\s\+$//e
  call cursor(l, c)
endfun
autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()
" }}} - Editing Behavior

" Folding settings - {{{
set nofoldenable                    " don't close folds when opening files
set foldcolumn=2                    " add a fold column
set foldmethod=marker               " detect triple-{ fold markers
set foldopen=block,hor,insert,jump,mark,percent,quickfix,search,tag,undo
                                    " which commands trigger unfold

function! MyFoldText()
    let line = getline(v:foldstart)

    let nucolwidth = &fdc + &number * &numberwidth
    let windowwidth = winwidth(0) - nucolwidth - 3
    let foldedlinecount = v:foldend - v:foldstart

    " expand tabs into spaces
    let onetab = strpart('          ', 0, &tabstop)
    let line = substitute(line, '\t', onetab, 'g')

    let line = strpart(line, 0, windowwidth - 2 -len(foldedlinecount))
    let fillcharcount = windowwidth - len(line) - len(foldedlinecount) - 4
    return line . ' …' . repeat(" ",fillcharcount) . foldedlinecount . ' '
endfunction
set foldtext=MyFoldText()
" }}} - Folding settings

" Editor settings - {{{
set termencoding=utf-8
set encoding=utf-8
" set lazyredraw                      " don't update display while running macros
set laststatus=2                    " always show status line
set cmdheight=2                     " 2-line status bar
set textwidth=99

if v:version >= 703
    set colorcolumn=79              " draw a column at col 79
endif

" statusline from
" https://github.com/nvie/vimrc/blob/master/vim/after/plugin/statusline.vim
if exists('g:loaded_fugitive')
   set statusline=%f\ %m\ (%L\ lines)\ %r\ %=%{fugitive#statusline()}\ (%l,%c)\ %y
else
   set statusline=%f\ %m\ (%L\ lines)\ %r\ %=(%l,%c)\ %y
endif

" Reformat XML
function! DoPrettyXML()
  " save the filetype so we can restore it later
  let l:origft = &ft
  set ft=
  " delete the xml header if it exists. This will
  " permit us to surround the document with fake tags
  " without creating invalid xml.
  1s/<?xml .*?>//e
  " insert fake tags around the entire document.
  " This will permit us to pretty-format excerpts of
  " XML that may contain multiple top-level elements.
  0put ='<PrettyXML>'
  $put ='</PrettyXML>'
  silent %!xmllint --format -
  " xmllint will insert an <?xml?> header. it's easy enough to delete
  " if you don't want it.
  " delete the fake tags
  2d
  $d
  " restore the 'normal' indentation, which is one extra level
  " too deep due to the extra tags we wrapped around the document.
  silent %<
  " back to home
  1
  " restore the filetype
  exe "set ft=" . l:origft
endfunction
command! PrettyXML call DoPrettyXML()
nnoremap <leader>x :PrettyXML<CR>

" Special configuration for Vagrantfiles
au BufRead,BufNewFile Vagrantfile call SetupVagrantfile()
au BufRead,BufNewFile README call SetupReSTFile()

function! SetupVagrantfile()
    setfiletype ruby
    set tabstop=2
    set softtabstop=2
    set shiftwidth=2
endfunction

function! SetupReSTFile()
    setfiletype rst
endfunction
" }}} - Editor settings

" Vim behavior - {{{
set hidden                          " hide buffers instead of closing them
set switchbuf=useopen               " reveal already opened files from the
                                    " quickfix window instead of opening new
                                    " buffers
set history=1000                    " command/search history length
set undolevels=1000                 " mucho undos por favor
if v:version >= 703
    set undofile                    " persistent undo file
    set undodir=~/.vim/.undo,~/tmp,/tmp
endif
set nobackup                        " no backup files (pita)
set noswapfile                      " no swap files (pita!)
set directory=~/.vim/.tmp,~/tmp,/tmp
                                    " put swap files here IF swapfile is
                                    " enabled

set viminfo='20,<50,s10,h,%         " remember some stuff after quiting vim:
                                    "     marks, registers, searches, buffer list
set wildmenu                        " bash-like tab complete for files/buffers
set wildmode=list:full              " show a list when hitting tab and complete
                                    " first full match
set wildignore=*.swp,*.bak,*.pyc,*.class
set title
set visualbell                      " no beeping!
set noerrorbells                    " no beeping!
set showcmd                         " show partial command in the last line
                                    " of the screen
set nomodeline                      " disable modelines for security
set cursorline                      " draw a line below the cursor

" tame quickfix window (toggle with ,f)
" nmap <silent> <leader>f :QFix<CR>

command! -bang -nargs=? QFix call QFixToggle(<bang>0)
function! QFixToggle(forced)
  if exists("g:qfix_win") && a:forced == 0
    cclose
    unlet g:qfix_win
  else
    copen 10
    let g:qfix_win = bufnr("$")
  endif
endfunction
" }}} - Vim behavior


" Highlighting - {{{

set background=dark " default to dark background
colorscheme koehler

if &t_Co >= 256 || has("gui_running")
    " color scheme for 256 colors
    colorscheme koehler
endif

if &t_Co > 2 || has("gui_running")
    syntax on                       " enable syntax highlighting when the term
                                    " has colors
endif

" slower but better sync, should fix problems with incorrect highlighting
" autocmd BufEnter * :syntax sync fromstart

" }}} - Highlighting

" Shortcuts {{{

" use ; for commands in addition to :
nnoremap ; :

" map <F1> to <Esc>
map! <F1> <Esc>

" close the current window, fast!
nnoremap <leader>q :q<CR>

" use Q to format the current paragraph (or selection)
vmap Q gq
nmap Q gqap

" replace visual selection with yank register when you hit p
vnoremap p <Esc>:let current_reg = @"<CR>gvdi<C-R>=current_reg<CR><Esc>

" swap ' and ` for marker jumps
" by default, ' goes to the line, ` goes to line + column
" line + column is more useful, so let's swap the keys
nnoremap ' `
nnoremap ` '

" remap j and k to work as expected on long, wrapped lines
nnoremap j gj
nnoremap k gk

" easier moving between windows
" use ,w to cycle through
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
nnoremap <leader>w <C-w>v<C-w>l

" complete whole filenames/lines a bit faster in insert mode
imap <C-f> <C-x><C-f>
imap <C-l> <C-x><C-l>

" use ,d (or ,dd ,dj 20,dd etc) to delete a line and skip the yank stack
nmap <silent> <leader>d "_d
vmap <silent> <leader>d "_d

" quick yank to end of line
nmap Y y$

" Yank/paste to OS clipboard with ,y and ,p
nmap <leader>y "+y
nmap <leader>Y "+yy
nmap <leader>p "+p
nmap <leader>P "+P

" set yankring history directory
let g:yankring_history_dir = '$HOME/.vim/.tmp'

" map ,r to show/hide the yank ring
nmap <leader>r :YRShow<CR>

" edit/source vimrc with ,ev and ,sv
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

" use ,n to clear search highlighting
nmap <silent> <leader>n :nohlsearch<CR>

" use ,s to enable/disable showing invisible chars
nmap <silent> <leader>s :set list!<CR>

" use ,e to open buf explorer
nmap <silent> <leader>e :BufExplorer<CR>

" jj/jk for getting out of insert mode
inoremap jj <Esc>
inoremap jk <Esc>

" text alignment
nmap <leader>al :left<CR>
nmap <leader>ar :right<CR>
nmap <leader>ac :center<CR>

" start a substitution using word under cursor
nmap <leader>z :%s#\<<C-r>=expand("<cword>")<CR>\>#

" quick-load of the scratch plugin
nmap <leader><tab> :Sscratch<CR><C-W>x<C-J>

" forgot to sudo vim?
cmap w!! w !sudo tee % > /dev/null

" use tab to jump between pairs
nnoremap <Tab> %
vnoremap <Tab> %

" use space for folding
nnoremap <Space> za
vnoremap <Space> za

" strip all trailing whitespace with ,W
nnoremap <leader>W :%s/\s\+$//<CR>:let @/=''<CR>

" strip all Windows line-endings
nnoremap <leader>m :%s/\r$//

" quick ack!
nnoremap <leader>a :Ack<Space>

" Reselect just-pasted text
nnoremap <leader>v V`]

" ReST titles
nmap <leader>= yyp:s/./=/<cr>
nmap <leader>- yyp:s/./-/<cr>
nmap <leader>~ yyp:s/./\~/<cr>
nmap <leader>^ yyp:s/./^/<cr>
nmap <leader>* yyp:s/./*/<cr>

" Insert ReST label
nmap <leader>l yyP:s/\(.*\)/.. _\1:/<CR>o<Esc><CR>

" Auto-correct these common misspellings:
abbreviate teh the
abbreviate adn and
abbreviate Todo TODO:
abbreviate todo TODO:

" Django: wrap the text inside an HTML tag with the `trans` template tag
" nmap <leader>t :s/>\([A-Za-z0-9 -_]\+\)</>{% trans "\1" %}</<cr>   " 1 line
" nmap <leader>gt :%s/>\([A-Za-z0-9  -_]\+\)</>{% trans "\1" %}</<cr> " entire buffer

" Django: wrap the src attribute within an img tag with the `static` template tag
nmap <leader>st :s/<img.*src="\([A-Za-z0-9-_\/\.]\+\)"/<img src="{% static "\1" %}"/   " 1 line
nmap <leader>gst :%s/<img.*src="\([A-Za-z0-9-_\/\.]\+\)"/<img src="{% static "\1" %}"/ " entire buffer

" Insert an ipdb breakpoint
nnoremap <leader>i oimport ipdb; ipdb.set_trace()<Esc>:w<cr>

" }}} - Shortcuts

augroup invisible_chars "{{{
    au!

    " Show invisible characters in all of these files
    autocmd filetype vim setlocal list
    autocmd filetype python,rst setlocal list
    autocmd filetype ruby setlocal list
    autocmd filetype javascript,css setlocal list
augroup end "}}}

" check for file changes on disk and prompt to reload
:au CursorHold * checktime

" restore cursor position when reopening files
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif

" extra vi compatibility - {{{
"set cpoptions+=$        " when changing a line, don't redisplay it, but put a $ at
"                        " the end
"set formatoptions-=o    " don't start new lines with a comment leader when
"                        " pressing o
"au filetype vim set formatoptions-=o
" }}} - extra vi compatibility


" ----------------------------------------------------------------------------
" vim-go
" ----------------------------------------------------------------------------

" settings
set autowrite
let g:go_fmt_autosave = 1
let g:go_fmt_command = "goimports"
let g:go_addtags_transform = "camelcase"
let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck']
let g:go_metalinter_autosave = 1
let g:go_metalinter_deadline = "5s"
let g:go_auto_sameids = 1
let g:go_auto_type_info = 1
set updatetime=100  " milliseconds

" syntax highlighting
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1

" shortcuts
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>
autocmd FileType go nmap <leader>r  <Plug>(go-run)
autocmd FileType go nmap <leader>t  <Plug>(go-test)
autocmd FileType go nmap <leader>T  <Plug>(go-test-func)
autocmd FileType go nmap <Leader>c  <Plug>(go-coverage-toggle)
autocmd FileType go nmap <Leader>c  <Plug>(go-coverage-toggle)
autocmd FileType go nmap <Leader>i <Plug>(go-info)
autocmd Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
autocmd Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
autocmd Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
autocmd Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')

" run :GoBuild or :GoTestCompile based on the go file
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#test#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction

autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>
" ----------------------------------------------------------------------------
