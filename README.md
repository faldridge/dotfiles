Quick Start
===========
This repository contains basic configuration for developing software with
[Vim][Vim] and a bootstrap script for initializing that configuration on a new
machine.

Invoking the bootstrap script triggers three primary actions:

1. Installs [Homebrew][Homebrew] and with it, a few system packages (Mac/OSX only), if
   these do not already exist on the system.
2. Installs a few globally useful Python packages system-wide.
3. Symlinks the dotfiles from the repository into their appropriate locations
   in the user's $HOME directory, for all dotfiles that are not already present.
4. Installs the [Vim][Vim] plugin manager, [Vundle][Vundle], and uses it to
   install the [Vim][Vim] plugins defined in the associated `vimrc` file.

Usage:

    git clone git@bitbucket.org:faldridge/dotfiles.git
    ./dotfiles/bootstrap.sh


[Vim]: http://www.vim.org/
[Homebrew]: http://brew.sh/
[Vundle]: https://github.com/gmarik/Vundle.vim
