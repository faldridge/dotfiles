# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# Git completion.
source ~/.git-completion.sh

# Git branch info on prompt.
source ~/.git-prompt.sh
PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '

# don't put duplicate lines in the history. See bash(1) for more options
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias ls='ls -GFh'

# Find all Python bytecode files in the current directory and its
# subdirectories and remove them.
alias nukepyc='find . -name "*.pyc" -exec rm -rf {} \;'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Various tools use these.
export PAGER=/usr/bin/less
export EDITOR=/usr/local/bin/vim

# Give priority to any binaries in ~/bin/
export PATH=~/bin:/usr/local/bin:$PATH

# Search command history like a god!
bind '"\e[A"':history-search-backward
bind '"\e[B"':history-search-forward

# For local development
export PYTHONDONTWRITEBYTECODE=1
source /usr/local/bin/virtualenvwrapper.sh

# Local overrides
if [ -f ~/.bashrc.local ]; then
    source ~/.bashrc.local
fi

# brew bash completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
    . $(brew --prefix)/etc/bash_completion
fi
